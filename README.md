# Signal Groups Reviews

The idea of this repository is to share the experience with various signal groups.

For good enough groups we can share a recommended cornix configuration and our monthly reports based on our experience.
We can also compare their fake reports with our real experience to find out how much are the official reports misleading
and what is the reality.

## Good Groups

- Binance Killers (BK)
- Fed Russian Insiders (FRI)
  - Same as Binance Killers?
- Bitcoin Bullets
  - Some affiliation with BK and FRI

- Bullstar
- Coinsignals
- Wallstreet Queen

- Crypto Inner Circle <https://t.me/cryptoinnercircle>
  - Very well handled even dangerous market situations
  - Experienced trader
  - Human made signals

- AltSignals Low Leverage <http://altsignals.io/>
  - Results published here: <https://www.altsignals.io/results>
  - They overshoot their results as everyone else does
  - But they are profitable and they have been pretty much all the time
  - Average ROI of signals is about 450% per month, total ROI is depending on actual investment
  - Recommended investment is 5% per signal - in that case, ROI is 22.5% per month
  - Human made signals
  - They update signals when situation changes
  - They use SL

- MCW - Mega Crypto World - good, VIP Channel SMC
- CryptoKey Club <https://t.me/crypto_key_signals>

## Dangerous groups

- BitsTurtle <https://bitsturtle.com/>
  - ***!!! Doesn't use SL !!!!***
  - promises to make about 10% ROI
  - in a good market conditions, it works
  - in a bad market conditions, liquidation is near (or maybe even imminent)
  - very bad risk-reward ratio 100% risk vs 10% reward
  - suffers from cornix bugs when multiple trades with the same coins are open
  - difficult to follow reporting - something must be followed on cornix, something on bybit, one must watch everything
  - on the other hand, admins are very helpful

## Bad Groups

- Binance master
- Scalping 300 <https://t.me/Scalping_300>
  - same as MCW Premium (VIP Channel)
- GG Shot <https://t.me/doublegtrading>
- Alex Friedman crypto futures
- Crypto Devil
  - scammer, deletes SL
- Jacob Crypto Bury <https://t.me/Jacobcryptobury_Signals>
- Fortune VIP

## Groups to be tested

## Groups on the radar

- Sublime
- WCSE
  - Worse than Binance Killers
- KAT Crypto Street <https://t.me/katcryptostreet>
  - Someone says good, someone else says bad
  - Has [Copy Trading on BingX](https://bingx.com/en-us/CopyTrading/1164673816908779525/?type=&apiIdentity=1185033748341235718)
- Daisy Signals Queen
  - Probably has some affiliation with KAT Crypto Premium
- Crypto rise
- Medabot

## Tools for testing signal groups

- <https://github.com/CryptobotCZ/crypto-signals-analysis>
